import java.util.Scanner;

public class E18 {
	private static int[] n;
	private static int aux,i,x;
	private static Scanner teclado;
	
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		n = new int[5];
		System.out.println("Informe 5 numeros inteiros: ");
		for(i = 0;i<5;i++)
			n[i] = teclado.nextInt();
		System.out.println("Os 5 numeros em ordem decrescente são: ");

		for(i = 0;i<5;i++)
			for(x = 0;x<5;x++) {
				if(n[i]>n[x]) {
					aux = n[i];
					n[i] = n[x];
					n[x] = aux;
				}
		}
		for(i = 0;i<5;i++) System.out.println(", "+n[i]);
	}

}
