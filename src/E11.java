import java.util.Scanner;

public class E11 {
	private static Scanner teclado;
	private  static int sexo;
	private static double altura;
	
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		
		System.out.println("Informe seu sexo com 1 para homem e 2 para mulheres: ");
		sexo = teclado.nextInt();
		System.out.println("Informe sua altura: ");
		altura = teclado.nextDouble();
		System.out.println("o peso ideal para você é: "+calcula(sexo, altura));
	}
	
	static protected double calcula(int sexo,double altura){
		
		if(sexo == 1) return (72.70*altura)-58;
		else if(sexo == 2) return 62.1*altura-44.7;
		else return -1;
	}

}
