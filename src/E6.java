import java.util.Scanner;

public class E6 {
	private static float kmini,kmfin,lucro,kmrod,vtotalrec;
	private static int npassageiros;
	private static Scanner teclado;
	public static void main(String args[]) {
		teclado = new Scanner(System.in);
		System.out.println("Informe o km inicial: ");
		kmini = teclado.nextFloat();
		System.out.println("Informe o km final: ");
		kmfin = teclado.nextFloat();
		System.out.println("Informe a quantidade de passageiros: ");
		npassageiros = teclado.nextInt();
		kmrod = (kmfin-kmini);
		System.out.println("Informe o valor total recebido: ");
		vtotalrec = teclado.nextFloat();
		lucro = (float) (vtotalrec - kmrod*1.9);
		System.out.println("O lucro líquido é: "+lucro);
		System.out.println("A media de lucro é: "+lucro/npassageiros);
		System.out.println("O valor gasto com combustível é: "+kmrod*1.9);
	}
}
