import java.util.Scanner;

public class E19 {
	private static int cod,n1,n2;
	private static Scanner teclado;
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		
		System.out.println("Informe a operação: "
				+ "1.Adição\n" + 
				", \n" + 
				"2.Subtração\n" + 
				", \n" + 
				"3.Divisão\n" + 
				",\n" + 
				"4.Multiplicação");
		cod = teclado.nextInt();
		System.out.println("Informe dois numeros: ");
		n1= teclado.nextInt();
		n2 = teclado.nextInt();
		
		switch(cod) {
			case 1: System.out.println(n1+n2); break;
			case 2: System.out.println(n1-n2); break;
			case 3: System.out.println(n1/n2); break;
			case 4: System.out.println(n1*n2); break;
			default : System.out.println(" Operação inválida! ");
		}

	}

}
