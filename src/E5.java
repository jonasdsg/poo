import java.util.Scanner;

public class E5 {
	private static double result,larg,comp,altu;
	private static Scanner teclado;
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		System.out.println("Informe a largura da cozinha: ");
		larg = teclado.nextDouble();
		System.out.println("Informe o comprimento da cozinha: ");
		comp = teclado.nextDouble();
		System.out.println("Informe o tamanho da parede da cozinha: ");
		altu = teclado.nextDouble();
		System.out.println("Ira colocar azulejos no teto? [s/n]");
		if(teclado.next().equals("s")){
			result = (comp*altu*2)+(larg*altu*2)+(2*larg*comp);
		}
		else result = (comp*altu*2)+(larg*altu*2)+(larg*comp);
		System.out.println("A quantidade de azulejos é: "+result/15);
	}

}
