import java.util.Scanner;

public class E22 {
	private static double n1,n2,resultado;
	private static Scanner teclado;
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		System.out.println("Informe o primeiro n�mero: ");
		n1 = teclado.nextDouble();
		do {
			System.out.println("Informe o segundo n�mero: ");
			n2 = teclado.nextDouble();
			if(n2==0) System.out.println("Valor inv�lido!");
		}while(n2==0);
		resultado = n1/n2;
		System.out.println("O valor resultante �: "+resultado);
		
	}

}
