import java.util.Scanner;

public class E23 {
	private static double n1,n2,resultado;
	private static Scanner teclado;
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		System.out.println("Informe a primeira nota: ");
		do {
			n1 = teclado.nextDouble();
			if(!(n1>=0 && n1<=10) ) System.out.println("Valor inv�lido!");
		}while(!(n1>=0 && n1<=10) );
		System.out.println("Informe a segunda nota: ");
		do {
			n2 = teclado.nextDouble();
			if(!(n2>=0 && n2<=10) ) System.out.println("Valor inv�lido!");
		}while(!(n2>=0 && n2<=10) );
		resultado = (n1+n2)/2;
		System.out.println("A m�dia �: "+resultado);
		
	}

}
