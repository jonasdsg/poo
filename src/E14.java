import java.util.Scanner;

public class E14 {
	private static int numero;
	private static Scanner teclado;
	
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		System.out.println("Informe o número de lados do poligono: ");
		numero = teclado.nextInt();
		if(numero==3) System.out.println("Triangulo");
		else 
			if(numero == 4) System.out.println("Retangulo");
			else if(numero==5) System.out.println("Pentagono");
		if(numero<3) System.out.println("Não é um polígono ");
		else if (numero>5) System.out.println("Polígono não identificado.");

	}

}