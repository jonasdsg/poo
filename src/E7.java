import java.util.Scanner;

public class E7 {

	private static double tampista,nvoltas,comptotal,consumo,reabastecimentos,totalgasto,tamanhotanq;
	private static Scanner teclado;
	public static void main(String args[]) {
		teclado = new Scanner(System.in);
		System.out.println("Informe o tamanho da pista em metros: ");
		tampista = teclado.nextDouble();
		System.out.println("Informe numero de voltas: ");
		nvoltas = teclado.nextDouble();
		comptotal = (nvoltas*tampista)/1000;
		System.out.println("Informe a quantidade de reabastecimento desejadas: ");
		reabastecimentos = teclado.nextDouble();
		System.out.println("Informe o consumo do carro em litros/Km: ");
		consumo = teclado.nextDouble();
		totalgasto = comptotal*consumo;
		tamanhotanq = totalgasto/reabastecimentos;
		System.out.println("O tamanho do tamque deverá ser: "+tamanhotanq);
	}
}