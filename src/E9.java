import java.util.Scanner;

public class E9 {
	private static int nmacas;
	private static double ttgasto;
	private static Scanner teclado;
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		nmacas = teclado.nextInt();
		if(nmacas>0 && nmacas<12)
			ttgasto = nmacas*0.3;
		else if(nmacas>=12)
			ttgasto = nmacas*0.25;
		System.out.println("Total gasto: "+ttgasto);
	}

}
