import java.util.Scanner;

/*
 * Escreva um programa para calcular e imprimir o número de lâmpadas necessárias para iluminar um
 * determinado cômodo de uma residência. Dados de entrada: a potência da lâmpada utilizada (em
 * watts), as dimensões (largura e comprimento, em metros) do cômodo. Considere que a potência
 * necessária é de 18 watts por metro quadrado
 * 
 */

public class E4 {
	private static double area,larg,comp,potlamp,qtlamp;
	private static Scanner teclado;
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		System.out.println("Informe a largura do local: ");
		larg = teclado.nextDouble();
		System.out.println("Informe o comprimento do local: ");
		comp = teclado.nextDouble();
		area = comp*larg;
		System.out.println("Informe a potência das lampadas compradas: ");
		potlamp = teclado.nextDouble();
		qtlamp =(area*18)/potlamp;
		System.out.println("A quantidade necessária de lampadas para uma área de "+area+" m² são: "+qtlamp);
		

	}

}
