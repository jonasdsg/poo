import java.util.Scanner;

public class E12 {
	private static int numero;
	private static Scanner teclado;
	
	public static void main(String[] args) {
		teclado = new Scanner(System.in);
		System.out.println("Informe um número: ");
		numero = teclado.nextInt();
		if(numero%2==1) System.out.println("Numero impar");
		else System.out.println("Numero par");

	}

}
