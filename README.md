```mermaid
sequenceDiagram
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```

[![](https://mermaid.ink/img/pako:eNqFU71uwyAQfpUTU6s6L8CQLu3gpaqa1QuFc4OEweHHUhTl3YsDjRMbpx7vvr-7MyfCjUBCicNDQM3xTbIfy7pGQ_wY98YCVxK1T5WeWS-57Jn2ub5DO0iOy7YUsStbifYLe-NklDquidwiEibVN9vty70PhW-pBTwFh7bWramA75lG9WnNEB1tLZ5vBTLrolMKRKEd5eprCxK7hI0im3mYCZd41wGhdh9BKSiFGdOs6oB0oCPzFVa55UG4ReZxkag4yTjKP5MsbOejZ7_bv2O-88V1KTg2POIUgiUYarFy16XH5aSpXN-xHm-gxJh8MnqCkYp0aDsmRXw-p5HUEL_HDhsSQxOBLQvKN6TR5wgNvYjbehejO6EtUw4rwoI3u6PmhHob8A-Un2BGnX8BcZtJaw)](https://mermaid.live/edit#pako:eNqFU71uwyAQfpUTU6s6L8CQLu3gpaqa1QuFc4OEweHHUhTl3YsDjRMbpx7vvr-7MyfCjUBCicNDQM3xTbIfy7pGQ_wY98YCVxK1T5WeWS-57Jn2ub5DO0iOy7YUsStbifYLe-NklDquidwiEibVN9vty70PhW-pBTwFh7bWramA75lG9WnNEB1tLZ5vBTLrolMKRKEd5eprCxK7hI0im3mYCZd41wGhdh9BKSiFGdOs6oB0oCPzFVa55UG4ReZxkag4yTjKP5MsbOejZ7_bv2O-88V1KTg2POIUgiUYarFy16XH5aSpXN-xHm-gxJh8MnqCkYp0aDsmRXw-p5HUEL_HDhsSQxOBLQvKN6TR5wgNvYjbehejO6EtUw4rwoI3u6PmhHob8A-Un2BGnX8BcZtJaw)